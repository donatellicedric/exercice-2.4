package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge {

    private DigitalBadgeMetadata metadata;
    private File badge;
    private Date end;
    private Date begin;
    private String serial;

    /**
     * Constructeur
     *
     * @param s
     * @param begin
     * @param end
     * @param metadata
     * @param badge
     */
    public DigitalBadge(String s, Date begin, Date end, DigitalBadgeMetadata metadata, File badge) {
        this.metadata = metadata;
        this.badge = badge;
    }

    public DigitalBadge(){}

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return Objects.equals(metadata, that.metadata) && Objects.equals(badge, that.badge) && Objects.equals(end, that.end) && Objects.equals(serial, that.serial);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge);
    }

    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                ", end=" + end +
                ", serial='" + serial + '\'' +
                '}';
    }

    /**
     * renvoi la date begin
     * @return begin
     */
    public Date getBegin() {
        return begin;
    }

    /**
     * revoi la date end
     * @return end
     */
    public Date getEnd() {
        return end;
    }

    /**
     * renvoi le numéro serial
     * @return serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * change la date begin
     * @param begin
     */
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    /**
     * change la date end
     * @param end
     */
    public void setEnd(Date end) {
        this.end = end;
    }

    /**
     * change le numéro serial
     * @param serial
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }
    public boolean compareTo(int Id){
        return Id == metadata.getBadgeId();
    }
}
